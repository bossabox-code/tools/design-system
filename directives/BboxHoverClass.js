export default {
    inserted: function(el, binding) {
        var classes = binding.value;
        el.toggleClasses = function() {
            for(var index in classes) {
                if(el.classList.contains(classes[index])) el.classList.remove(classes[index]);
                else el.classList.add(classes[index]);
            }
        }
        el.addEventListener('mouseover', el.toggleClasses);
        el.addEventListener('mouseout', el.toggleClasses);
    },
    unbind: function(el) {
        el.removeEventListener('mouseover', el.toggleClasses);
        el.removeEventListener('mouseout', el.toggleClasses);
    }
}